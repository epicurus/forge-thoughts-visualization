import api, { route, Route, APIResponse } from "@forge/api";

export const requestJiraJson = async (
    endpoint: Route,
    options?: { [key: string]: unknown }
): Promise<any> => {
    return (await api.asApp().requestJira(endpoint, options)).json();
};

export const requestConfluenceJson = async (
    endpoint: Route,
    options?: { [key: string]: unknown }
): Promise<any> => {
    return (await api.asApp().requestConfluence(endpoint, options)).json();
};
