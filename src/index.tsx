import _ from "lodash";
import { TriggerEvent, TriggerContext, Issue } from "./types";
import { updateThought } from "./updateThought";

type GroupedLink = {
    [key: string]: { prev: string[]; next: string[] };
};

const THOUGHT_STEP_ISSUE_TYPES = [
    "10005",
    "10006",
    "10007",
    "10008",
    "10009",
    "10010",
    "10011",
];

const CHANGE_TRIGGERS = ["summary", "description"];

export async function issueChange(
    event: TriggerEvent,
    context: TriggerContext
) {
    const {
        issue: {
            id: issueId,
            key: issueKey,
            fields: {
                issuetype: { id: issueTypeId },
                customfield_10014: epicLink,
            },
        },
        changelog,
        atlassianId,
    } = event;

    const items = changelog ? changelog.items : [];
    const issueChangedRelevant = items.reduce<boolean>(
        (acc, item) => CHANGE_TRIGGERS.includes(item.field) || acc,
        false
    );

    if (!issueChangedRelevant) {
        return;
    }

    await updateThought(issueId, issueKey, issueTypeId, atlassianId);
}
