import { requestConfluence, route } from "@forge/api";
import _ from "lodash";
import { FlattenedIssue } from "./types";
import { requestJiraJson, requestConfluenceJson } from "./common";

const SPACE_ID = 38928386;

type GroupedLinks = {
    [key: string]: { prev: string[]; next: string[] };
};
enum Direction {
    Prev = 1,
    Next,
}

type Chain = string[];

const THOUGHT_STEP_ISSUE_TYPES = [
    "10005",
    "10006",
    "10007",
    "10008",
    "10009",
    "10010",
    "10011",
];

const TRANSITION_TYPES = [
    { issueTypeId: "10005", id: "71", name: "1 Fragen und Rätsel" },
    { issueTypeId: "10006", id: "81", name: "2 Grundgedanken und Richtungen" },
    { issueTypeId: "10007", id: "91", name: "3 Erlebnisse" },
    { issueTypeId: "10008", id: "101", name: "4 Gedankenfehler und Gedanke" },
    { issueTypeId: "10009", id: "111", name: "5 Praktische Schritte" },
    { issueTypeId: "10010", id: "121", name: "6 Verhältnis zum Ganzen" },
    { issueTypeId: "10011", id: "131", name: "7 Wesen und Bedeutung" },
];

const CHANGE_TRIGGERS = ["summary", "description"];

const getFittingLink = (
    issueType: string,
    link: any,
    next: boolean
): boolean => {
    const index = THOUGHT_STEP_ISSUE_TYPES.indexOf(issueType);

    const {
        [link.outwardIssue ? "outwardIssue" : "inwardIssue"]: {
            fields: {
                issuetype: { id: linkType },
            },
        },
    } = link;
    return next
        ? index < THOUGHT_STEP_ISSUE_TYPES.length &&
              linkType === THOUGHT_STEP_ISSUE_TYPES[index + 1]
        : index > 0 && linkType === THOUGHT_STEP_ISSUE_TYPES[index - 1]
        ? true
        : false;
};

const traverseStepChain = (links: GroupedLinks, entry: string) => {
    const chains: Chain[] = [[entry]];
    const makeNewChain = (chain: Chain, step: string, direction: Direction) => {
        if (direction === Direction.Prev) {
            chain.unshift(step);
        } else {
            chain.push(step);
        }
        chains.push(chain);

        stepChain(step, chain, direction);
    };

    const stepChain = (entry: string, chain: Chain, direction: Direction) => {
        const newChain = [...chain];
        if (direction === Direction.Prev) {
            if (links[entry] && links[entry].prev.length > 0) {
                links[entry].prev.forEach((step, i) => {
                    if (i === 0) {
                        chain.unshift(step);
                        stepChain(step, chain, Direction.Prev);
                    } else {
                        makeNewChain(newChain, step, Direction.Prev);
                    }
                });
            } else if (links[entry]) {
                stepChain(chain[chain.length - 1], chain, Direction.Next);
            }
        } else {
            if (links[entry]) {
                links[entry].next.forEach((step, i) => {
                    if (i === 0) {
                        chain.push(step);
                        stepChain(step, chain, Direction.Next);
                    } else {
                        makeNewChain(newChain, step, Direction.Next);
                    }
                });
            }
        }
    };

    stepChain(entry, chains[0], Direction.Prev);

    return chains;
};

export async function updateThought(
    issueId: string,
    issueKey: string,
    issueTypeId: string,
    atlassianId: string
): Promise<{
    ok: boolean;
    error?: string;
}> {
    if (!THOUGHT_STEP_ISSUE_TYPES.includes(issueTypeId)) {
        return;
    }

    const {
        fields: {
            customfield_10014: epicLink,
            status: { name: issueStatus },
            issuetype: { name: issueType },
        },
    } = await requestJiraJson(
        route`/rest/api/3/issue/${issueId}?fields=customfield_10014,status,issuetype`
    );

    if (issueType !== issueStatus) {
        const transition = TRANSITION_TYPES.find(
            (transition) => transition.issueTypeId === issueTypeId
        );
        const body = JSON.stringify({
            transition: {
                id: transition.id,
            },
        });

        await requestJiraJson(route`/rest/api/3/issue/${issueId}/transitions`, {
            method: "post",
            body,
        });
    }

    const thoughtSteps = (
        await requestJiraJson(route`/rest/api/3/expression/eval`, {
            method: "post",
            body: JSON.stringify({
                expression:
                    "{ issues: issues.map(issue => ({summary: issue.summary, issueType: issue.issueType.id, description: issue.description.plainText, key: issue.key })) }",
                context: {
                    issues: {
                        jql: {
                            maxResults: 200,
                            query: `project = 'Allgemeine Gedankenfehler' AND 'Epic Link' = ${epicLink} AND reporter = ${atlassianId}`,
                            startAt: 0,
                            validation: "strict",
                        },
                    },
                },
            }),
        })
    ).value.issues as FlattenedIssue[];

    const rawLinks = await Promise.all(
        thoughtSteps.map(async (step) => {
            return requestJiraJson(
                route`/rest/api/3/issue/${step.key}?fields=issuelinks,issuetype`
            );
        })
    );

    const groupedLinks = rawLinks.reduce<GroupedLinks>((acc, link) => {
        const {
            key,
            fields: {
                issuelinks: issueLinks,
                issuetype: { id: issueType },
            },
        } = link;

        const prev = issueLinks
            .filter((link) => getFittingLink(issueType, link, false))
            .map((link) =>
                link.outwardIssue ? link.outwardIssue.key : link.inwardIssue.key
            );

        const next = issueLinks
            .filter((link) => getFittingLink(issueType, link, true))
            .map((link) =>
                link.outwardIssue ? link.outwardIssue.key : link.inwardIssue.key
            );

        if (prev.length || next.length) {
            if (acc[key]) {
                acc[key].prev.push(...prev);
                acc[key].next.push(...next);
            } else {
                acc[key] = {
                    prev,
                    next,
                };
            }
        }

        return acc;
    }, {});

    const thoughtChains = traverseStepChain(groupedLinks, issueKey);

    console.log(11.1, { thoughtChains, issueKey });

    const remoteLinks = await requestJiraJson(
        route`/rest/api/3/issue/${epicLink}/remotelink`
    );

    const confluenceUrl = remoteLinks.reduce(
        (acc, link) =>
            acc || link.relationship === "Wiki Page" ? link.object.url : "",
        ""
    );

    console.log(11.2, { confluenceUrl });

    const match = confluenceUrl.match(/pageId=(\d+)/);

    if (!match) {
        return {
            ok: false,
            error: "I didn't find a linked confluence page.",
        };
    }

    const [, confluencePageId] = match;

    console.log(11.3, { confluencePageId });

    const {
        page: { results: availablePages },
    } = await requestConfluenceJson(
        route`/wiki/rest/api/content/${confluencePageId}/child?expand=page`
    );

    console.log(11.4, { availablePages });

    await Promise.all(
        availablePages.map(async (page) => {
            const { results } = await requestConfluenceJson(
                route`/wiki/rest/api/content/${page.id}/property?key=thought-chain`
            );

            const { value: thoughtChainString } = results.find(
                (prop) => prop.key === "thought-chain"
            );

            page.thoughtChainString = thoughtChainString;
        })
    );

    console.log(11.5, { availablePages });

    const childPagesThoughtChains = thoughtChains.reduce<string[]>(
        (acc, chain) => [
            ...acc,
            chain.map((step) => step.match(/\d+/)).join("."),
        ],
        []
    );

    console.log(11.6, { childPagesThoughtChains });

    const childPages = await Promise.all(
        childPagesThoughtChains.map(async (thoughtChain, index) => {
            const availablePage = availablePages.find(
                (page) => thoughtChain === page.thoughtChainString
            );

            console.log(11.61, { availablePage });

            let newPage;

            if (!availablePage) {
                newPage = await requestConfluenceJson(
                    route`/wiki/rest/api/content`,
                    {
                        method: "post",
                        body: JSON.stringify({
                            title: `Variante ${index + 1} (${thoughtChain})`,
                            type: "page",
                            space: { key: "AG" },
                            body: {
                                storage: {
                                    value: "<string>",
                                    representation: "wiki",
                                },
                            },
                            ancestors: [
                                {
                                    id: confluencePageId,
                                },
                            ],
                        }),
                    }
                );

                console.log(11.62, { newPage });

                const ret = await requestConfluenceJson(
                    route`/wiki/rest/api/content/${newPage.id}/property`,
                    {
                        method: "post",
                        body: JSON.stringify({
                            key: "thought-chain",
                            value: thoughtChain,
                        }),
                    }
                );

                console.log(11.63, { ret });
            }

            return (
                availablePage || {
                    id: newPage.id,
                    type: newPage.type,
                    status: newPage.status,
                    title: newPage.title,
                }
            );
        })
    );

    console.log(11.7, { childPages });

    await Promise.all(
        childPages.map(async (page, i) => {
            const text = thoughtChains[i].reduce<string>((acc, step) => {
                const { summary, description } = thoughtSteps.find(
                    (thoughtStep) => thoughtStep.key === step
                );

                return `${acc}h3. ${summary}\n${description}\n\n`;
            }, "");

            const {
                version: { number },
            } = await requestConfluenceJson(
                route`/wiki/rest/api/content/${page.id}`
            );

            console.log(11.72, { text });

            return requestConfluenceJson(
                route`/wiki/rest/api/content/${page.id}`,
                {
                    method: "put",
                    body: JSON.stringify({
                        version: {
                            number: number + 1,
                        },
                        title: page.title,
                        type: "page",
                        body: {
                            storage: {
                                value: text,
                                representation: "wiki",
                            },
                        },
                    }),
                }
            );
        })
    );

    // console.log(15.9, {
    //     childPages,
    //     thoughtChains,
    //     thoughtSteps,
    // });
}
