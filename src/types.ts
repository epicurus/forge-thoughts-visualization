import type { Typed } from "adf-builder/dist/nodes";
/**
 * General Jira types
 */
type IssueType = {
    self: string;
    id: string;
    description: string;
    iconUrl: string;
    name: string;
    subtask: boolean;
    avatarId: number;
    hierarchyLevel: number;
};

type Project = {
    self: string;
    id: string;
    key: string;
    name: string;
    projectTypeKey: string;
    simplified: boolean;
    avatarUrls: {
        "48x48": string;
        "24x24": string;
        "16x16": string;
        "32x32": string;
    };
};

type IssueStatusCategory = {
    self: string;
    id: number;
    key: string;
    colorName: string;
    name: string;
};

type IssueStatus = {
    self: string;
    description: string;
    iconUrl: string;
    name: string;
    id: string;
    statusCategory: IssueStatusCategory;
};

type Comment = {
    comments: {
        self: string;
        id: string;
        author: {
            self: string;
            accountId: string;
            avatarUrls: {
                "48x48": string;
                "24x24": string;
                "16x16": string;
                "32x32": string;
            };
            displayName: string;
            active: boolean;
            timeZone: string;
            accountType: string;
        };
        body: {
            version: number;
            type: string;
            content: [
                { type: string; content: [{ type: string; text: string }] }
            ];
        };
        updateAuthor: {
            self: string;
            accountId: string;
            avatarUrls: {
                "48x48": string;
                "24x24": string;
                "16x16": string;
                "32x32": string;
            };
            displayName: string;
            active: boolean;
            timeZone: string;
            accountType: string;
        };
        created: string;
        updated: string;
        jsdPublic: boolean;
    }[];
    self: string;
    maxResults: number;
    total: number;
    startAt: number;
};

type Description = { version: number; type: string; content: Typed };

type IssueLink = {
    id: string;
    self: string;
    type: {
        id: string;
        name: string;
        inward: string;
        outward: string;
        self: string;
    };
    inwardIssue: {
        id: string;
        key: string;
        self: string;
        fields: {
            summary: string;
            status: {
                self: string;
                description: string;
                iconUrl: string;
                name: string;
                id: string;
                statusCategory: {
                    self: string;
                    id: number;
                    key: string;
                    colorName: string;
                    name: string;
                };
            };
            priority: {
                self: string;
                iconUrl: string;
                name: string;
                id: string;
            };
            issuetype: {
                self: string;
                id: string;
                description: string;
                iconUrl: string;
                name: string;
                subtask: true;
                avatarId: number;
                hierarchyLevel: number;
            };
        };
    };
};

type Fields = {
    summary: string;
    issuetype: IssueType;
    creator: {
        accountId: string;
    };
    created: string;
    updated: string;
    project: Project;
    reporter: {
        accountId: string;
    };
    assignee: {
        accountId: string;
    };
    status: IssueStatus;
    description: Description;
    comment: Comment;
    issuelinks?: IssueLink[];
    [key: string]: any;
};

type ChangeLog = {
    id: string;
    items: {
        field: "summary";
        fieldtype: "jira";
        fieldId: "summary";
        from: null;
        fromString: "test4c";
        to: null;
        toString: "test4d";
    }[];
};

export type Issue = {
    id: string;
    key: string;
    fields: Fields;
    self?: string;
    expand?: string;
};

export type FlattenedIssue = {
    issueType: number;
    summary: string;
    description: string;
    key: string;
};

export type TriggerEvent = {
    issue: Issue;
    atlassianId: string;
    changelog: ChangeLog;
    associatedUsers: {
        accountId: string;
    }[];
    context: {
        installContext: string;
    };
};

export type TriggerContext = {
    installContext: string;
};
